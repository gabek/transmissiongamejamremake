# TransmissionGameJamRemake

A remake of an old game that I made in unity for a game jam.

* Based off of Pac-man.
* Art assets and sound effects are not made by me and were acquired through open source websites and unity store.
* This is not to be distributed for profit.
* To play
	* WASD to move.
	* Pick up objects by running into them.
		* Picked up objects will slow you down.
	* Drop Objects with right click.
	* Transmit object at a Transmitter with left click.
		* This will transfer your carrying to score.
	* Avoid the Hunters/Ninjas.