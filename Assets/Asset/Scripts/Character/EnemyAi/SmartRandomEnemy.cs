﻿using System;
using UnityEngine;

/**
 * An AI that will wander unless the player is within a certain
 * distance of it. Based on Clyde from the original Pacman.
 * 
 * SuperClass: AbstractEnemy.
 */

public class SmartRandomEnemy : AbstractEnemy
{
    private readonly float Switch = 6f;

    /**
     * A Follow method that wanders the maze randomly until the Player is within 6 tiles of the Hunter.
     * The hunter than begins to follow the Player until they are more than 6 tiles away. Uses both
     * the SmartFollow and RandomFollow method. If there is no Player object provided defaults to RandomFollow.
     * 
     * @param current The current node the Hunter is at.
     * @param previous The previous node the Hunter was at.
     * @param player The gameobject representing the Player.
     * @param hunter The current Vector2 location of the Hunter.
     * 
     * @return HunterNode The next hunter node to go to.
     **/
    public override HunterNode Follow(HunterNode current, HunterNode previous, GameObject player, Vector2 hunter)
    {
        Rigidbody2D rigid = player.GetComponent<Rigidbody2D>();

        if (rigid != null)
        {
            Vector2 target = rigid.position;
            float distance = Math.Abs(Vector2.Distance(hunter, target));

            if (distance<Switch)
            {
                return SmartFollow(current, previous, target, hunter);
            }
            else
            {
                return RandomFollow(current, previous);
            }
        }
        else
        {
            return RandomFollow(current, previous);
        }
    }

    
}