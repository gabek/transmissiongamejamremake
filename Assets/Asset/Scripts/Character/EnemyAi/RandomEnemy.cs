﻿using UnityEngine;

/**
 * An AI that will just wanderer the maze for all time.
 * Used for debugging the maze's Node Graph.
 * 
 * SuperClass: AbstractEnemy.
 */
class RandomEnemy : AbstractEnemy
{
    /**
     * A Follow method that wanders the maze randomly for all eterninty. Doesn't ever attempt to target the player.
     * Uses the RandomFollow method.
     * 
     * @param current The current node the Hunter is at.
     * @param previous The previous node the Hunter was at.
     * @param player The gameobject representing the Player.
     * @param hunter The current Vector2 location of the Hunter.
     * 
     * @return HunterNode The next hunter node to go to.
     **/
    public override HunterNode Follow(HunterNode current, HunterNode previous, GameObject player, Vector2 hunter)
    {
        return RandomFollow(current, previous);
    }


}

