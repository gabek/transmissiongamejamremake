﻿using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * Author: Gab Kelly
 * An Abstract Class to be used as the basis of the AI for this game.
 * 
 * Super class: ICommonEnemy.
 * Sub Classes: RandomEnemy, FollowEnemy, AmbushEnemy, SmartRandomEnemy.
 */

public abstract class AbstractEnemy : ICommonEnemy
{

    public abstract HunterNode Follow(HunterNode current, HunterNode previous, GameObject player, Vector2 hunter);

    /**
     * A Method for wandering the maze in a random direction.
     * 
     * @param current The current node the Hunter is at.
     * @param previous The node the Hunter was previously at.
     * 
     * @return HunterNode The node for the Hunter to got to.
     */
    protected HunterNode RandomFollow(HunterNode current, HunterNode previous)
    {
        List<HunterNode> nextList = RemovePrevious(current, previous);
        System.Random random = new System.Random();
        int next = random.Next(0, nextList.Count);
        return nextList[next];
    }

    /**
     * A Method for getting to a specific point in the maze. Will base its decision on which node moves it closer to the target.
     * 
     * TODO: Add a depth to the call?
     * 
     * @param current The node the Hunter is currently at.
     * @param previous The node the Hunter was previously at.
     * @param target The target Vector2 the Hunter wants to reach.
     * @param hunter The Vector2 the Hunter is currently at.
     * 
     * @return HunterNode The node for the Hunter to go to.
     */
    protected HunterNode SmartFollow(HunterNode current, HunterNode previous, Vector2 target, Vector2 hunter)
    {
        List<HunterNode> nextList = RemovePrevious(current, previous);

        if (nextList.Count == 1) return nextList[0];

        float xTarg = 0;
        float yTarg = 0;
        (xTarg, yTarg) = DirectionToTarget(hunter, target);

        List<HunterNode> nextSelect = SelectNode(current, nextList, xTarg, yTarg);

        if (nextSelect.Count == 1) return nextSelect[0];

        System.Random random = new System.Random();
        int next = random.Next(0, nextSelect.Count);
        return nextSelect[next];
    }

    /**
     * Removes the node the hunter was previously at from the list of adjacent nodes in current.
     * 
     * @param current The node the Hunter is currently at.
     * @param previous The node the Hunter was previously at.
     * 
     * @param List The list of Hunter nodes without the previous node.
     */
    private List<HunterNode> RemovePrevious(HunterNode current, HunterNode previous)
    {
        List<HunterNode> nextList = new List<HunterNode>(current.AdjacentNodes);
        if (nextList.Contains(previous))
        {
            bool removed = nextList.Remove(previous);
            if (removed)
                MonoBehaviour.print("Removed the previous node: " + previous.name);
            else
                MonoBehaviour.print("Failed to remove previous Node: " + previous.name);
        }
        return nextList;
    }

    /**
     * Gets the Direction to a specified target
     * 
     * 
     * @param hunter The Vector2 that the hunter is at.
     * @param target The Vector2 that the target is at.
     * 
     * @return (float, float) A tuple of integers representing the direction to the target
     * in X and Y coordinates.
     */
    private (float, float) DirectionToTarget(Vector2 hunter, Vector2 target)
    {
        float xTarg = 0;
        float yTarg = 0;

        /*
        if (hunter.x > target.x)
            xTarg = -1;
        else if (hunter.x < target.x)
            xTarg = 1;
        if (hunter.y > target.y)
            yTarg = -1;
        else if (hunter.y < target.y)
            yTarg = 1;
        */
        xTarg = target.x - hunter.x;
        yTarg = target.y - hunter.y;
        return (xTarg, yTarg);
    }

    /**
     * Decision making method for narrowing down a list of nodes to select
     * which one to go to.
     * 
     * @param current The node the Hunter is curretly at.
     * @param nextList The list of next possible nodes to go to.
     * @param xTarg The X coordinate direction to the target.
     * @param yTarg The Y coordinate direction to the target.
     * 
     * @return List The list of selected nodes that are best possible potentials to the target.
     */
    private List<HunterNode> SelectNode(HunterNode current, List<HunterNode> nextList, float xTarg, float yTarg)
    {
        List<HunterNode> nextSelect = new List<HunterNode>();

        float maxScore = int.MinValue;
        float currentX = current.transform.position.x;
        float currentY = current.transform.position.y;

        foreach (HunterNode nextNode in nextList)
        {
            int xTemp = 0;
            int yTemp = 0;

            if ( currentX > nextNode.transform.position.x)
                xTemp = -1;
            else if (currentX < nextNode.transform.position.x)
                xTemp = 1;
            else if (currentY > nextNode.transform.position.y)
                yTemp = -1;
            else if (currentY < nextNode.transform.position.y)
                yTemp = 1;

            float score = Math.Abs(xTemp + xTarg) + Math.Abs(yTemp + yTarg);
            if (score > maxScore)
            {
                maxScore = score;
                nextSelect = new List<HunterNode>
                {
                    nextNode
                };
            }
            else if (score == maxScore)
            {
                nextSelect.Add(nextNode);
            }
        }

        return nextSelect;
    }
}
