﻿using UnityEngine;

/**
 * Author: Gab Kelly
 * 
 * An AI to Ambush the player. Will target a space 5 in front of
 * the player. Based on Pinky's AI from the original Pacman.
 * 
 * SuperClass: AbstractEnemy.
 */

class AmbushEnemy : AbstractEnemy
{
    private readonly int Lead = 5;


    /**
     * A Follow method that attempts to ambush the player by targeting the space
     * 5 tiles ahead of them. Uses the SmartFollow method. If there is
     * no Player object provided defaults to RandomFollow.
     * 
     * @param current The current node the Hunter is at.
     * @param previous The previous node the Hunter was at.
     * @param player The gameobject representing the Player.
     * @param hunter The current Vector2 location of the Hunter.
     * 
     * @return HunterNode The next hunter node to go to.
     **/
    public override HunterNode Follow(HunterNode current, HunterNode previous, GameObject player, Vector2 hunter)
    {
        Rigidbody2D rigid = player.GetComponent<Rigidbody2D>();
        PlayerController controller = player.GetComponent<PlayerController>();
        if (rigid != null && controller != null)
        {

            Vector2 playerPosition = rigid.position;
            Vector2 adjust = controller.Direction * Lead;
            Vector2 target = playerPosition + adjust;

            return SmartFollow(current, previous, target, hunter);
        }
        else
        {
            return RandomFollow(current, previous);
        }
    }
}

