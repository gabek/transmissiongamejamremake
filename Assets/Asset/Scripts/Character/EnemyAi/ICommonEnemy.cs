﻿using UnityEngine;

/**
 * The common enemy Interface.
 * 
 * Subclass: AbstractEnemy.
 */

public interface ICommonEnemy 
{
    HunterNode Follow(HunterNode current, HunterNode previous, GameObject player, Vector2 hunter); 
}
