﻿using UnityEngine;

/**
 * Author: Gab Kelly
 * 
 * An AI to follow the player no matter where they are.
 * Pretty standard AI. Mimics Blinky from the original Pacman.
 * 
 * SuperClass: AbstractEnemy.
 */

class FollowEnemy : AbstractEnemy
{
    /**
     * A Follow method that targets the player and attempts to follow them wherever they may go. Uses the SmartFollow method.If there is
     * no Player object provided defaults to RandomFollow.
     * 
     * @param current The current node the Hunter is at.
     * @param previous The previous node the Hunter was at.
     * @param player The gameobject representing the Player.
     * @param hunter The current Vector2 location of the Hunter.
     * 
     * @return HunterNode The next hunter node to go to.
     **/
    public override HunterNode Follow(HunterNode current, HunterNode previous, GameObject player, Vector2 hunter)
    {
        Rigidbody2D rigid = player.GetComponent<Rigidbody2D>();

        if (rigid != null)
        {
            Vector2 target = rigid.position;

            return SmartFollow(current, previous, target, hunter);
        }
        else
        {
            return RandomFollow(current, previous);
        }
    }
}

