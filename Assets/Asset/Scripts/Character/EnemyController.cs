﻿using UnityEngine;

/**
 * @author: Gab Kelly
 * 
 * The Script for setting up and Controlling the Hunter.
 */

public class EnemyController : MonoBehaviour
{
    private Rigidbody2D hunter;
    private Vector2 direction;
    private readonly ICommonEnemy[] AiSelector =
    {
        new RandomEnemy(),
        new FollowEnemy(),
        new AmbushEnemy(),
        new SmartRandomEnemy()
    };
    private ICommonEnemy EnemyAi;
    private readonly Vector2[] InitialDirect =
    {
        new Vector2(1,0),
        new Vector2(0,1),
        new Vector2(-1,0),
        new Vector2(0,-1)
    };
    private HunterNode previous = null;
    private Animator animator;

    private float distance;
    private float prevDistance;

    public GameObject playerObject;
    public HunterNode current;
   
    public float speed;
    public int aiId;
    public int init;

    /**
     * Initiates the hunter's values.
     **/
    void Start()
    {
        hunter = GetComponent<Rigidbody2D>();
        if (aiId >= 0 && aiId <= 3)
            EnemyAi = AiSelector[aiId];
        else
            EnemyAi = AiSelector[0];
        if (init >= 0 && init <= 3)
            direction = InitialDirect[init];
        else
            direction = InitialDirect[0];
        distance = Vector2.Distance(hunter.transform.position, current.transform.position);
        prevDistance = distance;
        animator = GetComponent<Animator>();
    }

    /**
     * Calls Follow and handles moving between nodes.
     **/
    void FixedUpdate()
    {
        if (playerObject != null)
        {
            distance = Vector2.Distance(hunter.transform.position, current.transform.position);

            if (distance < .1f || distance >= prevDistance)
            {
               
                HunterNode nextNode;
                nextNode = EnemyAi.Follow(current, previous, playerObject, hunter.position);

                int xTemp = 0;
                int yTemp = 0;

                if (current.transform.position.x > nextNode.transform.position.x)
                    xTemp = -1;
                else if (current.transform.position.x < nextNode.transform.position.x)
                    xTemp = 1;
                else if (current.transform.position.y > nextNode.transform.position.y)
                    yTemp = -1;
                else if (current.transform.position.y < nextNode.transform.position.y)
                    yTemp = 1;


                direction = new Vector2(xTemp, yTemp);
                previous = current;
                current = nextNode;
                distance = Vector2.Distance(hunter.transform.position, nextNode.transform.position);
            }
            animator.SetFloat("Direction", direction.x);
            prevDistance = distance;
            hunter.velocity = direction * speed;
        }
        else
        {
            hunter.velocity = 0 * direction;
        }
    }
}
