﻿using System.Collections.Generic;

/**
 * Author: Gab Kelly
 * 
 * An Object to Contain Data about the player.
 **/

public class Player
{
    private Stack<Item> itemStack;

    public int Score { get; private set; }
    public int Carrying { get; private set; }
    public float MaxCarry { get; }
    public float Weight { get; private set; }
    

    /**
     * Constructs a player object with a max carry.
     * 
     * @param maxCarry How much can the player object carry.
     **/
    public Player(int maxCarry)
    {
        this.Score = 0;
        this.Carrying = 0;
        this.MaxCarry = maxCarry;
        CalculateWeight();
        this.itemStack = new Stack<Item>();
    }
    
    /**
     * Adds the current carrying to the score.
     * Also clears the item stack of the player, and calculates the players
     * weight.
     *
     **/
    public void AddToScore()
    {
        Score = Score + Carrying;
        Carrying = 0;
        itemStack = new Stack<Item>();
        CalculateWeight();
    }

    /**
     * Attempts to add the carrying capacity. Will return a boolean
     * based on if it was able to or not. If a new carrying value is calculated
     * will calculate the new weight.
     * 
     * @param add The amount to be added to carrying.
     * @return bool If the amount to be added causes the value of carrying to be greater
     * than the maxCarry value return false. Else return true and update carrying.
     */
    public bool ModifyCarry(int add)
    {
        if ((add + Carrying) > MaxCarry)
        {
            return false;
        }
        else
        {
            Carrying = Carrying + add;
            CalculateWeight();
            return true;
        }
    }

    /**
     * Pushes the item to a stack. Functionally equivellant to
     * @link{Stack} Push().
     * 
     * @param item The item to be added to the stack.
     **/
    public void PushStack(Item item)
    {
        itemStack.Push(item);
    }

    /**
     * Pops an item off the stack. Will also call @link{ModifyCarry}
     * based on the items score to change the value of carrying.
     * 
     * @return Item Returns the item that was popped off of the stack.
     **/
    public Item PopStack()
    {
        Item item = itemStack.Pop();
        ModifyCarry(-1*item.Score);
        return item;
    }


    /**
     * Peeks at the item at the top of the itemStack. Functionally
     * equivellant to @link{Stack} Peek.
     * 
     * @return Item The item at the top of the Stack.
     **/
    public Item PeekStack()
    {
        return itemStack.Peek();
    }

    /**
     * Gets Number of Items that are in itemStack. Functionally
     * equivellant to @link{Stack} Count.
     * 
     * @return int The number of items in the Stack.
     **/
    public int CountItems()
    {
        return itemStack.Count;
    }
    private void CalculateWeight()
    {
        Weight = (MaxCarry / (MaxCarry + Carrying));
    }
}
