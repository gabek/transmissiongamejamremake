﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Author: Gab Kelly
 * 
 * The controller for the main player in my game jam.
 * Handles things like movement, dropping items, playing audio, and picking up stuff.
 * 
 * TODO: Change how maxCarry is displayed so no matter maxCarry's value its %/100.
 */

public class PlayerController : MonoBehaviour {

    private Rigidbody2D knight;
    private AudioSource transmitAudio;
    private Dictionary<string, GameObject> itemPrefabs;
    private GameController GameController;
    private Animator animator;

    private float moveSpeed;
    private float dropWait;
    private Player PlayerData;
    private float carryWidth;

    public Vector2 Direction { get; private set; }
    public Text scoreText;
    public Image carryOverlay;
    public List<GameObject> prefabList;
    public GameObject GameMaster;

    public float speed;
    public int maxCarry;

    /**
     * Initiates the all the values of the PlayerController.
     **/
    void Start()
    {
        knight = GetComponent<Rigidbody2D>();
        PlayerData = new Player(maxCarry);
        moveSpeed = speed;
        dropWait = 1;
        itemPrefabs = new Dictionary<string, GameObject>();
        GameController = GameMaster.GetComponent<GameController>();
        foreach (GameObject prefab in prefabList)
        {
            Item item = prefab.GetComponent<Item>();
            if (item != null)
                itemPrefabs.Add(item.ItemName, prefab);
        }
        Direction = new Vector2(1, 0);
        transmitAudio = GetComponent<AudioSource>();
        CreateScoreText();
        animator = GetComponent<Animator>();
        carryWidth = carryOverlay.rectTransform.rect.width;
        CarryUi();
    }

    /**
     * Handles movement, button input, and updates to the player.
     **/
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        if (!Mathf.Approximately(movement.x, 0.0f) || !Mathf.Approximately(movement.y, 0.0f))
        {
            Vector2 TempDirect = new Vector2(movement.x, movement.y);
            TempDirect.Normalize();
            Direction = TempDirect;
        }
        moveSpeed = speed * PlayerData.Weight;

        knight.velocity = moveSpeed * movement;
        if (Input.GetMouseButton(1) && dropWait < Time.time)
        {
            Drop();
            dropWait = 1 + Time.time;
        }
        if (Input.GetMouseButton(0) && PlayerData.Carrying > 0)
        {
            Transmission();
        }

        animator.SetFloat("Direction", Direction.x);
        animator.SetFloat("Speed", movement.magnitude);
        animator.SetFloat("Weight", PlayerData.Weight);

        int tempScore = PlayerData.Score;

        tempScore = 0;

        GameController.VictoryCheck(PlayerData.Score);
    }

    /**
     * Transmits the items currently in the Player itemStack.
     * 
     * Will increase the Score of the Player.
     **/
    private void Transmission()
    {
        RaycastHit2D hit = Physics2D.Raycast(knight.position + Vector2.up * .2f, Direction, 1f, LayerMask.GetMask("Transmitter"));
        if (hit.collider != null)
        {
            transmitAudio.Play();
            PlayerData.AddToScore();
            CarryUi();
            CreateScoreText();
        }
    }

    /**
     * Drops an Item carried by the player.
     **/
    private void Drop()
    {
        if (PlayerData.CountItems() > 0)
        {
            if (itemPrefabs.ContainsKey(PlayerData.PeekStack().ItemName))
            {
                Item removed = PlayerData.PopStack();
                GameObject prefab = itemPrefabs[removed.ItemName];
                Object.Instantiate(prefab, new Vector2(this.transform.position.x, this.transform.position.y-.5f), Quaternion.identity);
                CarryUi();
            }
        }
        else
            print("No items");
    }

    /**
     * Creates the carryText based on the amount the player is currently carrying.
     **/
    private void CarryUi()
    {
        carryOverlay.rectTransform.SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, ((PlayerData.Carrying / PlayerData.MaxCarry) * carryWidth));
    }

    /**
     * Creates the scoreText based on the player's current score.
     **/
    private void CreateScoreText()
    {
        scoreText.text = "Score: " + PlayerData.Score;
    }

    /**
     * A method that triggers when entering another object. Used
     * to collect items.
     * 
     * @param Collider2D The Collider that the player entered.
     **/
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Item")
        {
            Item item = other.gameObject.GetComponent<Item>();
            bool success = PlayerData.ModifyCarry(item.Score);

            if (success)
            {
                PlayerData.PushStack(item);
                CarryUi();
                Destroy(other.gameObject);
            }
        }
    }

    /**
     * A method to handle collisions with other objects. Used to detect
     * if a player has connected with a Hunter (layer 11 is enemy).
     * 
     * @param Collision2D The Object the player has collided with.
     **/
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 11)
        {
            animator.SetBool("Death", true);
            GameController.GameOver(PlayerData.Score);
            Destroy(gameObject,.7f);
        }
    }

    


}
