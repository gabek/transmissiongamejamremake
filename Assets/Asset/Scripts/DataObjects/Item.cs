﻿using UnityEngine;

/**
 * An Item for the player to pick up in the maze.
 * Used to win the game.
 * 
 * As a note. The lack of proper Getter and Setter design for the
 * variables is to make them work with Unity's prefabs.
 * 
 * TODO: Add a flicker/transparency to an item that cannot be
 * picked up yet.
 */

public class Item : MonoBehaviour
{
    private float enableTime;

    public int Score;
    public string ItemName;

    /**
     * Sets the time it gets enabled to 1 second after Start is called.
     */
    void Start()
    {
        enableTime = Time.time + 1;
    }

    /**
     * Enables the item after one second.
     */
    private void FixedUpdate()
    {
        if(enableTime < Time.time)
            GetComponent<BoxCollider2D>().enabled = true;
    }
}

