﻿using System.Collections.Generic;
using UnityEngine;

/**
 * 
 * Author: Gab Kelly
 * 
 * A Node that a Hunter uses to traverse the maze.
 * 
 * Contains a list of Adjacent nodes.
 * 
 * The use of Nodes was based upon: https://noobtuts.com/unity/2d-pacman-game
 */

public class HunterNode : MonoBehaviour
{
    public List<HunterNode> AdjacentNodes;
   
}
