﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Author: Gab Kelly
 * Main Game handler.
 * 
 * Takes care of winning the game, losing the game, and then subsquently restarting the game
 */

public class GameController : MonoBehaviour
{
    public Text Display;

    public int Goal;

    private AudioSource cheer;

    private bool restart;
    private bool victory;
    private string restartText;

    /**
     * Initiates the Game's values.
     **/
    private void Start()
    {
        restart = false;
        victory = false;
        restartText = "\n Press 'R' to restart.";
        cheer = GetComponent<AudioSource>();
    }

    /**
     * Checks if the player has (and can) entered the button to restart
     * the game.
     **/
    private void FixedUpdate()
    {
        if (restart)
            if (Input.GetKeyDown(KeyCode.R))
                SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    /**
     * Called to check if the victory condition has been met.
     *
     * @param int The current score of the player. Must be equal to or greater than the goal.
     **/
    public void VictoryCheck(int score)
    {
        if(score>=Goal && !victory)
        {
            victory = true;
            restart = true;
            Display.text = "You've achieved the max score!\n Congratulations!" + restartText;
            cheer.Play();
        }
    }

    /**
     * Ends the game by displaying the game over screen.
     * 
     * @param int The current score of the player. It is displayed on screen when
     * they die.
     **/
    public void GameOver(int score)
    {
        if (!victory)
        {
            restart = true;
            Display.text = "You died!\n Final Score: " + score + restartText;
        }

    }
}